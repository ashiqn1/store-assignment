
import { GetData, PostData, DeleteData } from './api';



export const AddBrands = (data) => {
    const apiUrl = 'http://localhost:8080/api/brands';
    const customHeaders = {
    };
    return PostData(apiUrl, customHeaders, data);
};


export const GetBrands = (page = 1, limit = 10) => {
    const apiUrl = `http://localhost:8080/api/brands?page=${page}&limit=${limit}`;
    const customHeaders = {

    };
    return GetData(apiUrl, customHeaders);
};

export const DeleteBrands = (id) => {
    const apiUrl = `http://localhost:8080/api/brands/${id}`;
    const customHeaders = {

    };
    return DeleteData(apiUrl, customHeaders);
};


export const GetCategories = () => {
    const apiUrl = `http://localhost:8080/api/brands/categories`;
    const customHeaders = {
    };
    return GetData(apiUrl, customHeaders);
};

export const GetTerritories = () => {
    const apiUrl = `http://localhost:8080/api/brands/territories`;
    const customHeaders = {
    };
    return GetData(apiUrl, customHeaders);
};