import axios from 'axios';

export const GetData = async (url, headers) => {
  try {
    const response = await axios.get(url, {});
    return response.data;
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }
};


export const DeleteData = async (url, headers) => {
  try {
    const response = await axios.delete(url, {})
    return response.data;
  } catch (error) {
    console.error('Error deleting data:', error);
    throw error;
  }
};


export const PostData = async (url, headers, body) => {
  try {
    const response = await axios.post(url, body, {
      headers: {
        'Content-Type': 'application/json',
      }
    });

    return response.data;
  } catch (error) {
    console.error('Error posting data:', error);
    throw error;
  }
};
