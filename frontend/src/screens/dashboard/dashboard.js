import React, { useState, useEffect } from 'react';
import List from '../../components/list/list';
import { GetBrands, GetCategories, GetTerritories, DeleteBrands } from '../../api/store_api';
import Modal from 'react-modal';
import './dashboard.css';
import FormElem from '../../components/formElem/formElem'
import Pagination from '../../components/pagination/pagination';
import BrandTile from "../../components/tile/tile.js";
import Nike from '../../assets/nike.png';
import BrandA from '../../assets/brandA.png'
import BrandB from '../../assets/brandB.png'
import BrandC from '../../assets/brandC.png'
import BrandD from '../../assets/brandD.png'
import BrandE from '../../assets/brandE.png'
import BrandF from '../../assets/brandF.png'
import LeftSidebar from '../../components/leftbar/leftbar';

const ITEMS_PER_PAGE = 12;

const Dashboard = () => {
  const [brandData, setBrandData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [isAddBrandModalOpen, setIsAddBrandModalOpen] = useState(false);

  const [category, setCategory] = useState([]);
  const [territory, setTerritory] = useState([]);


  const images = [Nike, BrandA, BrandB, BrandC, BrandD, BrandE, BrandF];

  function getRandomImage() {
    const randomIndex = Math.floor(Math.random() * images.length);
    return images[randomIndex];
  }

  const handleBrandAdded = () => {
    setIsAddBrandModalOpen(false);
    GetBrands(currentPage, ITEMS_PER_PAGE)
      .then(response => {
        setBrandData(response.data);
        setTotalPages(Math.ceil(response.metadata.total / response.metadata.per_page));
      })
      .catch(error => {
        console.error('Error:', error);
      });
  };


  const handleDelete = (id) => {
    DeleteBrands(id).then(response => {
      setBrandData(prevBrandData => prevBrandData.filter(brand => brand.id !== id));
    })
      .catch(error => {
        console.error('Error:', error);
      });
  };


  const handleAddBrandClick = () => {
    setIsAddBrandModalOpen(true);
  };

  const handlePrevPage = () => {
    setCurrentPage(prevPage => Math.max(prevPage - 1, 1));
  };

  const handleNextPage = () => {
    setCurrentPage(prevPage => Math.min(prevPage + 1, totalPages));
  };
  useEffect(() => {
    GetBrands(currentPage, ITEMS_PER_PAGE)
      .then(response => {
        setBrandData(response.data);
        setTotalPages(Math.ceil(response.metadata.total / response.metadata.per_page));
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }, [currentPage]);

  useEffect(() => {
    GetCategories()
      .then(response => {
        if (response && response.data) {
          setCategory(response.data)
        }
      })
      .catch(error => {
        console.error('Error:', error);
      });


    GetTerritories()
      .then(response => {
        if (response && response.data) {
          setTerritory(response.data)
        }
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }, []);

  return (
    <>
    <LeftSidebar></LeftSidebar>
    <div className="dashboard-container">
  
     
      <button className="add-brand-button" onClick={handleAddBrandClick}>Add Brand</button>
      <h2>Brand List</h2>
      {/* <List brandData={brandData} onDelete={handleDelete} /> */}

      <div className="tile-list">
{brandData && brandData.length && brandData.map((brand) => (
  <BrandTile brandname={brand.name} Productname={brand.product_name} rating={4.5} location={brand.location} image={getRandomImage()} id={brand.id} onDelete={handleDelete}></BrandTile>
))}
 </div>
      <Pagination
        currentPage={currentPage}
        totalPages={totalPages}
        onPrevPage={handlePrevPage}
        onNextPage={handleNextPage}
      />

      <Modal
        isOpen={isAddBrandModalOpen}
        onRequestClose={() => setIsAddBrandModalOpen(false)}
        contentLabel="Add Brand Modal"
        className="modal"
        overlayClassName="overlay"
      >
        <div className="modal-content">
          <FormElem category={category} location={territory} onBrandAdded={handleBrandAdded}></FormElem>
        </div>
      </Modal>
    </div>
    </>
  );
};

export default Dashboard;
