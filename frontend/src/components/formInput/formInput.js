import React from 'react';
import { Controller } from 'react-hook-form';
import './formInput.css'

const FormInput = ({ text, name, control }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{text} :</label>
      <Controller
        name={name}
        control={control}
        defaultValue=""
        render={({ field }) => <input type="text" {...field} className="form-control" />}
      />
    </div>
  );
};

export default FormInput;
