import React from 'react';
import './tile.css'; // Import CSS for styling
import Close from '../../assets/close.png'


const BrandTile =(props) => {
    const handleDelete = (id) => {
        props.onDelete(id); // Call the onDelete function passed via props
    }
    
  return (
    <div className="brand-tile-container">
         {/* <img src={Close} className="close-icon" /> */}
      <img src={props.image} alt="brand image" />
      <div className="content-container brand-tile-content-container">
        <span className="title"><span className="user-icon">{props.brandname.substring(0, 2)}</span>{props.Productname}</span>
        <div className="action-container">
        {/* <span className="user-icon">{brandname.substring(0, 2)}</span> */}
        <button className="close-icon"  onClick={() => handleDelete(props.id)}>Delete</button>
        </div>
        <div className="content">
              <span>Origin: {props.location}</span>
          </div>
        <div className="content">
              <span>Brand: {props.brandname}</span>
          </div>
        
      </div>
      <div className="user-rating-tile-container" name={props.Productname} rating={props.rating}></div>
    </div>
  );

 
};

export default BrandTile;
