import React from 'react';
import './pagination.css'

const Pagination = ({ currentPage, totalPages, onPrevPage, onNextPage }) => {
    return (
        <div className="pagination">
            <button
                onClick={onPrevPage}
                disabled={currentPage === 1}
            >
                Previous
            </button>
            <span>{currentPage}</span>
            <button
                onClick={onNextPage}
                disabled={currentPage === totalPages}
            >
                Next
            </button>
        </div>
    );
};

export default Pagination;
