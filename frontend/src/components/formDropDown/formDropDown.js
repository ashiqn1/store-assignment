
import React from 'react';
import { Controller } from 'react-hook-form';
import Select from 'react-select';
import './formDropDown.css';

const FormDropDown = ({ values, text, name, control }) => {
  const options = values.map(value => ({
    label: value.name,
    value: value.id,
  }));

  const defaultValue = options.length > 0 ? options[0] : null;

  return (
    <div className="form-group">
      <label htmlFor={name}>{text}:</label>
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue ? defaultValue.value : null} 
        render={({ field }) => (
          <Select
            {...field}
            options={options}
            onChange={(val) => field.onChange(val?.value)}
            value={options.find((c) => c.value === field.value)}
          />
        )}
      />
    </div>
  );
};

export default FormDropDown;



