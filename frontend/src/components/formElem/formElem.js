// AddBrandForm.js
import React,{useState} from 'react';
import { useForm } from 'react-hook-form';
import './formElem.css';
import { AddBrands } from '../../api/store_api';
import FormInput from '../formInput/formInput';
import FormDropDown from '../formDropDown/formDropDown';

const AddBrandForm = (props) => {
  const { handleSubmit, control, getValues, errors } = useForm();
  const [apiError, setApiError] = useState(null); 

  const onSubmit = (formData) => {
    const categoryId = getValues('category_id');
    const locationId = getValues('location_id');
    const updatedFormData = { ...formData, category_id: categoryId, location_id: locationId };

    AddBrands(updatedFormData)
      .then(response => {
        props.onBrandAdded();
      })
      .catch(error => {
        if (error.response && error.response.data && error.response.data.error && error.response.status==409) {
          setApiError(error.response.data.error);
        } else {
          setApiError('An error occurred while adding the brand. Please try again.');
        }
        console.error('Error:', error);
      });
  };

  return (
    <div className="add-brand-form">
      <h2>Add Brand Form</h2>
      {apiError && <div className="api-error">{apiError}</div>}
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormInput name='name' text='Brand Name' control={control} />
        <FormInput name='product_name' text='Product Name' control={control} />
        <FormDropDown name='category_id' text='Category' control={control} values={props.category} />
        <FormDropDown name='location_id' text='Location' control={control} values={props.location} />
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
};

export default AddBrandForm;
