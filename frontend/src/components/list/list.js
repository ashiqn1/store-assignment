
import React from "react";
import "./list.css"; // Import the CSS file




const List = (props) => {
 
  const handleDelete = (id) => {
    props.onDelete(id); // Call the onDelete function passed via props
  };
  return (
    <div className="brand-list-container">
      <h2>Brand List</h2>
      <table className="brand-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Product Name</th>
            <th>Category</th>
            <th>Location</th>
            <th>Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {props.brandData && props.brandData.length && props.brandData.map((brand) => (
            <tr key={brand.id}>
              <td>{brand.name}</td>
              <td>{brand.product_name}</td>
              <td>{brand.category}</td>
              <td>{brand.location}</td>
              <td>{brand.created_at}</td>
              <td>
                <button onClick={() => handleDelete(brand.id)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
     

    </div>
  );
};

export default List;
