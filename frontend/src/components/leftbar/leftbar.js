// LeftSidebar.js
import React from 'react';
import './leftbar.css'; // Import CSS for styling

const LeftSidebar = () => {
  return (
    <div className="left-sidebar">
      <nav>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Services</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
      </nav>
    </div>
  );
};

export default LeftSidebar;
