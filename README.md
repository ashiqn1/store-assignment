## Store Service Documentation

This documentation provides comprehensive information about the Store Service, offering details on setting it up, running it via Docker Compose, and essential usage guidelines.

### Table of Contents

Introduction

Setup and Installation

API Endpoints



### Introduction

The Store Service is a web application designed to manage a store. It provides functionalities related to brands, products, categories, and locations.
#### Frontend: Built with React, a popular and robust JavaScript library for building user interfaces.
#### Backend: Developed in Golang, providing speed, efficiency, and concurrency for optimal performance.
#### Database: PostgreSQL is utilized as the primary database for efficient and reliable data management.


## Setup and Installation

To set up and run the Store Service locally, follow these steps:

    git clone https://gitlab.com/ashiqn1/store-assignment.git
    cd store-assignment


Create a .env file to store the environment credentials:

    # .env file
    STORE_DB_USER=<user_name>
    STORE_DB_PASSWORD=<password>
    STORE_DB_DATABASE=<database_name>
    STORE_DB_PORT=<database_port>
    STORE_DB_SCHEMA=<schema>


Ensure Docker and Docker Compose are installed on your system.


Run the following command to start the services:

    docker-compose up --build -d



<br/>

**You can access the website in**

##        http://localhost:3000/


<br/>


To down the services

    docker-compose down


<br/>

## API Endpoints

The backend server runs on the default port 8080.

**Endpoints**

    POST    http://localhost:8080/api/brands
    GET     http://localhost:8080/api/brands
    DELETE  http://localhost:8080/api/brands/:id
    GET     http://localhost:8080/api/brands/categories
    GET     http://localhost:8080/api/brands/territories


These endpoints allow interaction with the Store Service, enabling various operations related to brands, categories, and more.

