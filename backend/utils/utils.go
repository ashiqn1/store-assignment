package utils

import (
	"fmt"
	"gocco-store/internal/entities"
	"strings"

	"gitlab.com/tuneverse/toolkit/consts"
)

func IsEmpty(str string) bool {
	return strings.TrimSpace(str) == ""
}

// CalculateOffset builds a pagination query offset based on the given page and limit.
func CalculateOffset(page, limit int) string {

	if page <= 0 {
		page = consts.DefaultPage
	}
	if limit <= 0 || limit > 25 {
		limit = consts.DefaultLimit
	}
	offset := fmt.Sprintf("%s %d %s %d", "LIMIT", limit, "OFFSET", (page-1)*limit)
	return offset
}

func MetaDataInfo(metaData entities.MetaData) entities.MetaData {
	if int64(metaData.CurrentPage)*int64(metaData.PerPage) < metaData.Total {
		metaData.Next = metaData.CurrentPage + 1
	}
	if metaData.CurrentPage > 1 {
		metaData.Prev = metaData.CurrentPage - 1
	}
	return metaData
}
