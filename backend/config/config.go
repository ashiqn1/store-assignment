package config

import (
	"fmt"
	"gocco-store/internal/entities"
	"os"
	"strconv"
)

// LoadConfig loads the configuration for the application based on the given appName.
// It uses environment variables and the "envconfig" package to populate the configuration struct.
// Parameters:
// - appName: The name of the application to load configuration for.
// Returns:
// - *entities.EnvConfig: A pointer to the populated configuration struct.
// - error: An error if there was an issue loading the configuration.
// func LoadConfig(appName string) (*entities.EnvConfig, error) {

// 	var cfg entities.EnvConfig

// 	err := godotenv.Load()
// 	if err != nil {
// 		return nil, fmt.Errorf("error loading .env file: %w", err)
// 	}

// 	err = envconfig.Process(appName, &cfg)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return &cfg, nil
// }

func LoadConfig() (*entities.EnvConfig, error) {
	var cfg entities.EnvConfig

	cfg.Db.User = os.Getenv("STORE_DB_USER")
	portStr := os.Getenv("STORE_DB_PORT")
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, fmt.Errorf("error parsing STORE_DB_PORT: %w", err)
	}
	cfg.Db.Port = port

	cfg.Db.Password = os.Getenv("STORE_DB_PASSWORD")
	cfg.Db.DATABASE = os.Getenv("STORE_DB_DATABASE")
	cfg.Db.Schema = os.Getenv("STORE_DB_SCHEMA")
	cfg.Db.Host = os.Getenv("STORE_DB_HOST")
	apPortStr := os.Getenv("STORE_PORT")
	apPort, err := strconv.Atoi(apPortStr)
	if err != nil {
		return nil, fmt.Errorf("error parsing STORE_DB_PORT: %w", err)
	}
	cfg.Port = apPort
	cfg.Debug = true

	return &cfg, nil
}
