
CREATE TABLE IF NOT EXISTS category (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO category (name)
VALUES 
    ('coffee'), 
    ('food'), 
    ('toys'), 
    ('appliances'), 
    ('furniture'),
    ('clothing'), 
    ('electronics'), 
    ('books'), 
    ('beauty'), 
    ('sports');

CREATE TABLE IF NOT EXISTS location (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE 
);

INSERT INTO location (name)
VALUES 
    ('Los Angeles'), 
    ('New York'), 
    ('Seattle'), 
    ('Dallas'), 
    ('Miami'),
    ('Chicago'), 
    ('San Francisco'), 
    ('Houston'), 
    ('Atlanta'), 
    ('Boston');


CREATE TABLE IF NOT EXISTS brand (
    id SERIAL PRIMARY KEY,
    brand_name VARCHAR(255) NOT NULL,
    product_name VARCHAR(255) NOT NULL,
    category_id INT REFERENCES category(id),
    location_id INT REFERENCES location(id),
    created_at TIMESTAMP DEFAULT NOW(),
    UNIQUE (brand_name, product_name)  
);




