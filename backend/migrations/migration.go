package migrations

import (
	"fmt"
	"gocco-store/config"
	"gocco-store/internal/consts"
	"gocco-store/internal/repo/driver"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

func Migration(operation string) {
	// init the env config
	cfg, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}
	// logrus init
	log := logrus.New()

	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database")
		return
	}
	driverDB, err := postgres.WithInstance(pgsqlDB, &postgres.Config{})
	if err != nil {
		panic(err)
	}
	// migration instance creation
	m, err := migrate.NewWithDatabaseInstance(
		"file://./migrations/scripts",
		consts.DatabaseType, driverDB)
	if err != nil {
		panic(err)
	}
	if operation == "up" {
		if err := m.Up(); err != nil {
			if err == migrate.ErrNoChange {
				fmt.Println("No migration to up")
			} else {
				panic(err)
			}
		}
	}
	if operation == "down" {
		if err := m.Down(); err != nil {
			if err == migrate.ErrNoChange {
				fmt.Println("No migration to down")
			} else {
				panic(err)
			}
		}
	}
	if err != nil && err != migrate.ErrNoChange {
		panic(err)
	}

	log.Printf("migration %v completed...", operation)

}
