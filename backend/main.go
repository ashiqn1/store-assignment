package main

import (
	"gocco-store/app"
	"gocco-store/migrations"
)

func main() {

	migrations.Migration("up")
	app.Run()
}
