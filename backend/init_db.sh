#!/bin/bash
set -e

/etc/init.d/postgresql start
psql --command "CREATE USER ${STORE_DB_USER} WITH SUPERUSER PASSWORD '${STORE_DB_PASSWORD}';"
createdb -O ${STORE_DB_USER} ${STORE_DB_DATABASE}

psql --command "CREATE TABLE IF NOT EXISTS category (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);"

psql --command "INSERT INTO category (name)
    VALUES 
        ('coffee'), 
        ('food'), 
        ('toys'), 
        ('appliances'), 
        ('furniture'),
        ('clothing'), 
        ('electronics'), 
        ('books'), 
        ('beauty'), 
        ('sports');"

psql --command "CREATE TABLE IF NOT EXISTS location (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);"

psql --command "INSERT INTO location (name)
    VALUES 
        ('Los Angeles'), 
        ('New York'), 
        ('Seattle'), 
        ('Dallas'), 
        ('Miami'),
        ('Chicago'), 
        ('San Francisco'), 
        ('Houston'), 
        ('Atlanta'), 
        ('Boston');"

psql --command "CREATE TABLE IF NOT EXISTS brand (
    id SERIAL PRIMARY KEY,
    brand_name VARCHAR(255) NOT NULL,
    product_name VARCHAR(255) NOT NULL,
    category_id INT REFERENCES category(id),
    location_id INT REFERENCES location(id),
    created_at TIMESTAMP DEFAULT NOW(),
    UNIQUE (brand_name, product_name)
);"
