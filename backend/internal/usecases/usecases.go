package usecases

import (
	"context"
	"gocco-store/internal/entities"
	"gocco-store/internal/repo"
	"gocco-store/utils"
	"log"

	"github.com/go-playground/validator/v10"
)

type StoreUseCases struct {
	repo repo.StoreRepoImply
}

type StoreUseCaseImply interface {
	AddBrand(ctx context.Context, brand entities.Brand) error
	GetBrands(ctx context.Context, reqData entities.ReqParams) ([]entities.Brand, error)
	GetTotalBrandCount(ctx context.Context) (int64, error)
	DeleteBrand(ctx context.Context, brandId int64) error
	GetAllCategory(ctx context.Context) ([]entities.Category, error)
	GetAllLocations(ctx context.Context) ([]entities.Location, error)
}

// NewStoreUseCases
func NewStoreUseCases(StoreRepo repo.StoreRepoImply) StoreUseCaseImply {
	return &StoreUseCases{
		repo: StoreRepo,
	}
}

func (store *StoreUseCases) AddBrand(ctx context.Context, brand entities.Brand) error {

	if err := validateBrand(brand); err != nil {
		log.Printf("AddBrand failed, error=%s", err.Error())
		return err
	}
	return store.repo.AddBrand(ctx, brand)
}

func validateBrand(brand entities.Brand) error {
	validate := validator.New()
	err := validate.Struct(brand)
	if err != nil {
		log.Printf("validateBrand failed, error=%s", err.Error())
		return err
	}
	return nil
}

func (store *StoreUseCases) GetTotalBrandCount(ctx context.Context) (int64, error) {
	// You can add the logic to query the total count from the repository here
	totalCount, err := store.repo.GetTotalBrandCount(ctx)
	if err != nil {
		log.Printf("GetTotalBrandCount failed, error=%s", err.Error())
		return 0, err
	}
	return totalCount, nil
}

func (store *StoreUseCases) GetBrands(ctx context.Context, reqData entities.ReqParams) ([]entities.Brand, error) {

	offsetQuery := utils.CalculateOffset(int(reqData.Page), int(reqData.Limit))
	subscriptionDuration, err := store.repo.GetBrands(ctx, offsetQuery)
	if err != nil {
		log.Printf("GetBrands failed, error=%s", err.Error())
		return nil, err
	}
	return subscriptionDuration, nil
}

func (store *StoreUseCases) DeleteBrand(ctx context.Context, brandId int64) error {
	return store.repo.DeleteBrand(ctx, brandId)
}

func (store *StoreUseCases) GetAllCategory(ctx context.Context) ([]entities.Category, error) {
	return store.repo.GetAllCategory(ctx)
}

func (store *StoreUseCases) GetAllLocations(ctx context.Context) ([]entities.Location, error) {
	return store.repo.GetAllLocations(ctx)
}
