package entities

import "time"

// EnvConfig represents the configuration structure for the application.
type EnvConfig struct {
	Debug bool     `default:"true" split_words:"true"` // Flag indicating debug mode (default: true)
	Port  int      `default:"8080" split_words:"true"` // Port for server to listen on (default: 8080)
	Db    Database `split_words:"true"`                // Database configuration
	//AcceptedVersions []string `required:"true" split_words:"true" default:"v1.0"` // List of accepted API versions (required)
}

// Database represents the configuration for the database connection.
type Database struct {
	User     string
	Password string
	Port     int
	Host     string
	DATABASE string
	Schema   string
	// Schema    string `envconfig:"default=public"`
	MaxActive int
	MaxIdle   int
}

//type Store struct{}

type Brand struct {
	ID           int       `json:"id,omitempty"`
	Name         string    `json:"name" validate:"required,min=4,lt=255"`
	ProductName  string    `json:"product_name" validate:"required,min=4,lt=255"`
	CategoryID   int       `json:"category_id,omitempty" validate:"required"`
	LocationID   int       `json:"location_id,omitempty" validate:"required"`
	CreatedAt    time.Time `json:"created_at,omitempty"`
	CategoryName string    `json:"category,omitempty"`
	Location     string    `json:"location,omitempty"`
}

type MetaData struct {
	Total       int64 `json:"total"`
	PerPage     int32 `json:"per_page"`
	CurrentPage int32 `json:"current_page"`
	Next        int32 `json:"next"`
	Prev        int32 `json:"prev"`
}

type ReqParams struct {
	Page       int32  `form:"page"`
	Limit      int32  `form:"limit"`
	SearchTerm string `form:"search_term"`
}

type Category struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
type Location struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
