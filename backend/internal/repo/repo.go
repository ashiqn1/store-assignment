package repo

import (
	"context"
	"database/sql"
	"fmt"
	"gocco-store/internal/entities"
	"log"

	"github.com/lib/pq"
)

type StoreRepo struct {
	db *sql.DB
}

type StoreRepoImply interface {
	AddBrand(ctx context.Context, brand entities.Brand) error
	GetBrands(ctx context.Context, subQuery string) ([]entities.Brand, error)
	GetTotalBrandCount(ctx context.Context) (int64, error)
	DeleteBrand(ctx context.Context, brandId int64) error
	GetAllCategory(ctx context.Context) ([]entities.Category, error)
	GetAllLocations(ctx context.Context) ([]entities.Location, error)
}

// NewStoreRepo
func NewStoreRepo(db *sql.DB) StoreRepoImply {
	return &StoreRepo{db: db}
}

// GetStore
func (store *StoreRepo) AddBrand(ctx context.Context, brand entities.Brand) error {

	_, err := store.db.ExecContext(ctx, `INSERT INTO brand (brand_name, product_name, category_id, location_id)
	VALUES ($1,$2,$3,$4)`, brand.Name, brand.ProductName, brand.CategoryID, brand.LocationID)
	if err != nil {
		log.Printf("AddBrand failed, error=%s", err.Error())
		pqErr, ok := err.(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" {
			return fmt.Errorf("unique violation")
		}
		return err
	}
	return nil
}

func (store *StoreRepo) GetBrands(ctx context.Context, subQuery string) ([]entities.Brand, error) {

	var brands []entities.Brand

	// var durations []entities.SubscriptionDuration
	selectQuery := fmt.Sprintf(`
	SELECT
		brand.id,
		brand.brand_name,
		brand.product_name,
		category.name AS category,
		location.name AS location,
		brand.created_at
	FROM
		brand
	JOIN
		category ON brand.category_id = category.id
	JOIN
		location ON brand.location_id = location.id `) + subQuery

	rows, err := store.db.QueryContext(ctx, selectQuery)
	if err != nil {
		log.Printf("GetBrands failed, error=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var brand entities.Brand
		if err := rows.Scan(&brand.ID, &brand.Name, &brand.ProductName, &brand.CategoryName, &brand.Location, &brand.CreatedAt); err != nil {
			log.Printf("GetBrands failed, error=%s", err.Error())
			return nil, err
		}
		brands = append(brands, brand)
	}

	if err := rows.Err(); err != nil {
		log.Printf("GetBrands failed, error=%s", err.Error())
		return nil, err
	}
	return brands, nil
}

// GetTotalSubscriptionDurationCount returns the total count of subscription durations in the database.
func (store *StoreRepo) GetTotalBrandCount(ctx context.Context) (int64, error) {
	var totalCount int64
	countQuery := "SELECT COUNT(*) FROM brand"
	err := store.db.QueryRowContext(ctx, countQuery).Scan(&totalCount)
	if err != nil {
		log.Printf("GetTotalBrandCount failed, error=%s", err.Error())
		return 0, err
	}
	return totalCount, nil
}

func (store *StoreRepo) DeleteBrand(ctx context.Context, brandId int64) error {
	result, err := store.db.Exec("DELETE FROM brand WHERE id = $1", brandId)

	// Check for no rows affected (no brand found for the given id)
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return fmt.Errorf("no row deleted")
	}

	return nil
}

func (store *StoreRepo) GetAllCategory(ctx context.Context) ([]entities.Category, error) {
	var categories []entities.Category

	selectQuery := fmt.Sprintf(`SELECT id,name FROM category`)

	rows, err := store.db.QueryContext(ctx, selectQuery)
	if err != nil {
		log.Printf("GetAllCategory failed, error=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var category entities.Category
		if err := rows.Scan(&category.ID, &category.Name); err != nil {
			log.Printf("GetAllCategory failed, error=%s", err.Error())
			return nil, err
		}
		categories = append(categories, category)
	}

	if err := rows.Err(); err != nil {
		log.Printf("GetAllCategory failed, error=%s", err.Error())
		return nil, err
	}
	return categories, nil
}

func (store *StoreRepo) GetAllLocations(ctx context.Context) ([]entities.Location, error) {
	var locations []entities.Location

	selectQuery := fmt.Sprintf(`SELECT id,name FROM location`)

	rows, err := store.db.QueryContext(ctx, selectQuery)
	if err != nil {
		log.Printf("GetAllLocations failed, error=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var location entities.Location
		if err := rows.Scan(&location.ID, &location.Name); err != nil {
			log.Printf("GetAllLocations failed, error=%s", err.Error())
			return nil, err
		}
		locations = append(locations, location)
	}

	if err := rows.Err(); err != nil {
		log.Printf("GetAllLocations failed, error=%s", err.Error())
		return nil, err
	}
	return locations, nil
}
