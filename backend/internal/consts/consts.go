package consts

// AppName stores the Application name
const (
	AppName          = "store"
	DatabaseType     = "postgres"
	AcceptedVersions = "v1.0"
)
const (
	DefaultPage  = 1
	DefaultLimit = 10
)
