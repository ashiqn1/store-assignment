package controllers

import (
	"fmt"
	"gocco-store/internal/entities"
	"gocco-store/internal/usecases"
	"gocco-store/utils"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type StoreController struct {
	router   *gin.RouterGroup
	useCases usecases.StoreUseCaseImply
}

// NewStoreController
func NewStoreController(router *gin.RouterGroup, StoreUseCase usecases.StoreUseCaseImply) *StoreController {
	return &StoreController{
		router:   router,
		useCases: StoreUseCase,
	}
}

// InitRoutes
func (store *StoreController) InitRoutes() {
	store.router.GET("/health", store.HealthHandler)
	store.router.POST("/brands", store.CreateBrand)
	store.router.GET("/brands", store.GetBrands)
	store.router.DELETE("/brands/:id", store.DeleteBrand)
	store.router.GET("/brands/territories", store.GetAllLocations)
	store.router.GET("/brands/categories", store.GetAllCategory)
}

// HealthHandler
func (store *StoreController) HealthHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

func (store *StoreController) CreateBrand(c *gin.Context) {

	var brand entities.Brand
	ctx := c.Request.Context()
	if err := c.Bind(&brand); err != nil {
		log.Printf("CreateBrand failed, error=%s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	if err := store.useCases.AddBrand(ctx, brand); err != nil {
		log.Printf("CreateBrand failed, error=%s", err.Error())
		var errMsg error
		if err.Error() == "unique violation" {
			errMsg = fmt.Errorf("Brand with the same brand name (%s) and product name (%s) already exists", brand.Name, brand.ProductName)
			c.JSON(http.StatusConflict, gin.H{
				"error": errMsg.Error(),
			})
			return
		}
		errMsg = fmt.Errorf("internal server error")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": errMsg.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "brand created successfully",
	})
}

func (store *StoreController) GetBrands(c *gin.Context) {
	ctx := c.Request.Context()

	var reqParam entities.ReqParams
	if err := c.BindQuery(&reqParam); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	totalCount, err := store.useCases.GetTotalBrandCount(ctx)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
		return
	}
	brands, err := store.useCases.GetBrands(ctx, reqParam)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
		return
	}

	// Calculate the total number of subscription durations
	currentPage := int(reqParam.Page)
	nextPage := currentPage + 1
	prevPage := currentPage - 1
	// Create and populate metadata
	metaData := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(reqParam.Limit),
		CurrentPage: int32(reqParam.Page),
		Next:        int32(nextPage),
		Prev:        int32(prevPage),
	}

	// Add metadata to the response
	response := gin.H{
		"message":  "brands retrieved successfully",
		"data":     brands,
		"metadata": utils.MetaDataInfo(metaData),
	}

	c.JSON(http.StatusOK, response)
}

func (store *StoreController) DeleteBrand(c *gin.Context) {
	ctx := c.Request.Context()
	brandIdStr := c.Param("id")
	if len(brandIdStr) < 1 {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "brand id is missing",
		})
		return
	}

	brandId, err := strconv.Atoi(brandIdStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "id is not valid",
		})
		return
	}
	if err := store.useCases.DeleteBrand(ctx, int64(brandId)); err != nil {
		if err.Error() == "no row deleted" {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": "no brand found for delete",
			})
			return
		}
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "brand id is missing",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "brand deleted successfully",
	})
}

func (store *StoreController) GetAllLocations(c *gin.Context) {
	ctx := c.Request.Context()
	locations, err := store.useCases.GetAllLocations(ctx)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
		return
	}

	response := gin.H{
		"message": "territories retrieved successfully",
		"data":    locations,
	}

	c.JSON(http.StatusOK, response)
}

func (store *StoreController) GetAllCategory(c *gin.Context) {
	ctx := c.Request.Context()
	category, err := store.useCases.GetAllCategory(ctx)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
		return
	}

	response := gin.H{
		"message": "categories retrieved successfully",
		"data":    category,
	}

	c.JSON(http.StatusOK, response)
}
