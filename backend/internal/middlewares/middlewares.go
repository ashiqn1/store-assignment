package middlewares

// Middlewares structure for storing middleware values
type Middlewares struct {
}

// NewMiddlewares creates a new middleware object
func NewMiddlewares() *Middlewares {
	return &Middlewares{}
}
